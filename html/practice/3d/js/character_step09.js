function Character(info) {

    this.mainElem = document.createElement( 'div' );
    this.mainElem.classList.add( 'character' );

    this.mainElem.innerHTML = ''
        + '<div class="character-face-con character-head">'
            + '<div class="character-face character-head-face face-front"></div>'
            + '<div class="character-face character-head-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-torso">'
            + '<div class="character-face character-torso-face face-front"></div>'
            + '<div class="character-face character-torso-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-right">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-left">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-right">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-left">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>';

    document.querySelector( '.stage' ).appendChild(this.mainElem);

    this.mainElem.style.left = info.xPos + '%';
    this.scrollState = false;
    this.lastScrollTop = 0;
    this.xPos = info.xPos;
    this.speed = 0.3;
    this.direction;

    // 1. 극단적으로 빨리지는 이유는 한 번만 눌러도 requestAnimationFrame 이 동작하는데
    // 여러번 누르면 중첩이 되서 연타하면 할수록 계속 빨라지게 된다. (여기서는 keydown 으로 제어)
    // 즉, 여기서는 keydown 이벤트가 반복되는 것을 막으면 해결할 수 있다.
    // 좌우 이동 중인지 아닌지를 체크하려고 함
    this.runningState = false;

    // 4-1. 키다운 후에 키업을 하더라도 좌우이동(애니메이션)하는 것을 멈추게 하기 위해
    // 키업 이벤트에 requestAnimationFrame 을 취소해도록 한다.
    this.rafId;

    this.init();
}

Character.prototype = {
    constructor: Character,
    init: function () {

        const self = this;

        window.addEventListener( 'scroll', function () {
            clearTimeout( self.scrollState );

            if (!self.scrollState) {
                self.mainElem.classList.add( 'running' );
            }

            self.scrollState = setTimeout(function () {
                self.scrollState = false;
                self.mainElem.classList.remove( 'running' );
            }, 500);

            if (self.lastScrollTop > pageYOffset) {
                self.mainElem.setAttribute( 'data-direction', 'backward' );
            } else {
                self.mainElem.setAttribute( 'data-direction', 'forward' );
            }

            self.lastScrollTop = pageYOffset;
        } );

        window.addEventListener('keydown', function (e) {

            // 2. 맨 처음에는 실행이 되게 하지만 그 다음부터는 실행을 하지 않음
            if (self.runningState) return;

            if (e.keyCode == 37) {
                self.mainElem.setAttribute( 'data-direction', 'left' );
                self.mainElem.classList.add( 'running' );
                self.direction = 'left';
                self.run(self);

                // 3-1. run 을 실행후 true 로 변경하고 keydown 을 한번만 실행하기 위함
                // 즉, 키를 계속 누르고 있어도 한 번만 실행한다.
                self.runningState = true;

            } else if (e.keyCode == 39) {
                self.mainElem.setAttribute( 'data-direction', 'right' );
                self.mainElem.classList.add( 'running' );
                self.direction = 'right';
                self.run(self);

                // 3-2.
                self.runningState = true;
            }

        });

        window.addEventListener( 'keyup', function () {
            self.mainElem.classList.remove( 'running' );

            // 4-3.
            cancelAnimationFrame( self.rafId );

            // 5. cancelAnimationFrame 이후에 self.runningState 값이 true 이므로
            // 키다운이 동작하지 않으므로 false 로 변경해 준다.
            // console.log( self.runningState );
            self.runningState = false;

        } );
    },
    run : function (self) {

        if (self.direction === 'left') {
            self.xPos -= self.speed;
        } else if (self.direction === 'right') {
            self.xPos += self.speed;
        }

        self.mainElem.style.left = self.xPos + '%';

        // 4-2.
        self.rafId = requestAnimationFrame(function () {
            self.run( self );
        })
    }
};
