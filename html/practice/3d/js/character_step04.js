function Character(info) {

    this.mainElem = document.createElement( 'div' );
    this.mainElem.classList.add( 'character' );

    this.mainElem.innerHTML = ''
        + '<div class="character-face-con character-head">'
            + '<div class="character-face character-head-face face-front"></div>'
            + '<div class="character-face character-head-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-torso">'
            + '<div class="character-face character-torso-face face-front"></div>'
            + '<div class="character-face character-torso-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-right">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-left">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-right">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-left">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>';

    document.querySelector( '.stage' ).appendChild(this.mainElem);

    this.mainElem.style.left = info.xPos + '%';

    // 1-1. 스크롤 중인지 아닌지를 체크하는 변수 선언
    this.scrollState = false; // this.scrollState;

    this.init();
}

Character.prototype = {
    constructor: Character,
    init: function () {
        const self = this;
        window.addEventListener( 'scroll', function () {
            // 3. 스크롤되자마자 setTimeout 의 값이 있던지 없던지 간에 무조건 클리어를 실행한다.
            // 아래에서 0.5초마다 setTimeout이 실행되도록 해 놓았지만
            // 스크롤이 계속 발생할 때마다 클리어를 해주고 있기 때문에
            // 스크롤이 멈춘 시점 0.5초 후에 self.mainElem.classList.remove( 'running' ); 를 실행하게 된다.
            clearTimeout( self.scrollState );

            // 1-2. 처음값이 false 이고, 스크롤 발생이 running 클래스를 붙인다.
            if (!self.scrollState) {
                self.mainElem.classList.add( 'running' );
                // 1-3. 실행이 계속되므로 성능을 저하시킬 수 있음
                console.log( 'running 클래스 붙었음' );
            }

            // 2. 0.5초마다 running 클래스를 제거한다.
            // 하지만 로직상 스크롤이 끝난 시점에 한번만 실행하게 된다.
            self.scrollState = setTimeout(function () {
                self.scrollState = false;
                self.mainElem.classList.remove( 'running' );
            }, 500);
            // setTimeout 은 실행할 때마다 고유 아이디값을 숫자로 반환한다.
            // console.log( self.scrollState );
            // if (!self.scrollState) 위의 조건문에서 처음 실행 후에 setTimeout 이 실행되면 0 이상의 숫자에 계속 부정연산자(!)로 인해
            // 계속 false 가 되므로 running 클래스가 계속 붙어서 성능 저하를 발생시키는 것을 최소화시킬 수 있다.
            // 즉, 0.5 초동안에 조건문 상태가 false 로 인해 running 을 붙이지 않는다.
            // 하지만 다시 0.5초 후에는 다시 self.scrollState = false; 로 인해 running 클래스를 붙이게 된다.
        } );
    }
};
