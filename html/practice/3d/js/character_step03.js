function Character(info) {

    this.mainElem = document.createElement( 'div' );
    // 캐릭터에 running 클래스가 붙으면 ani-running-leg, ani-running-arm 애니메이션이 동작하도록 한다.
    // 1. 테스트용
    // this.mainElem.classList.add( 'character', 'running' );
    this.mainElem.classList.add( 'character' );

    this.mainElem.innerHTML = ''
        + '<div class="character-face-con character-head">'
            + '<div class="character-face character-head-face face-front"></div>'
            + '<div class="character-face character-head-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-torso">'
            + '<div class="character-face character-torso-face face-front"></div>'
            + '<div class="character-face character-torso-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-right">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-left">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-right">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-left">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>';

    document.querySelector( '.stage' ).appendChild(this.mainElem);

    this.mainElem.style.left = info.xPos + '%';

    // 초기화 실행
    this.init();
}

// 2. wall3d.js 에도 scroll 이벤트가 있지만 스크롤시 캐릭터에 적용해야 하기 때문에 여기서 구현한다.
// 생성자함수를 통해 생성된 인스턴스 객체가 공통으로 사용하는 속성이나 메소드는 prototype 으로 만들어 준다.
// 걸어가는 기능은 모든 인스턴스가 가지고 있기 때문에

Character.prototype = {
    constructor: Character,
    init: function () {
        const self = this;
        window.addEventListener( 'scroll', function () {
            // 3-1. 에러가 발생할 것이다.
            // this.mainElem.classList.add( 'running' );

            // 3-2. this.mainElem 를 못찾은 것은 this 때문인데,
            // 여기서 이벤트핸들러에서 this 는 이벤트대상인 window 를 가리킨다.
            self.mainElem.classList.add( 'running' );
        } );
    }
};
