function Character(info) {

    this.mainElem = document.createElement( 'div' );
    this.mainElem.classList.add( 'character' );

    this.mainElem.innerHTML = ''
        + '<div class="character-face-con character-head">'
            + '<div class="character-face character-head-face face-front"></div>'
            + '<div class="character-face character-head-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-torso">'
            + '<div class="character-face character-torso-face face-front"></div>'
            + '<div class="character-face character-torso-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-right">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-left">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-right">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-left">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>';

    document.querySelector( '.stage' ).appendChild(this.mainElem);

    this.mainElem.style.left = info.xPos + '%';
    this.scrollState = false;
    this.lastScrollTop = 0;

    // 캐릭터의 위치는 left(%)값이기 때문에 이 값으로 움직이도록 한다.
    // xPos 로 위치를 선언하므로 xPos 값을 갱신하여 움직일 수 있다.
    // 6-1. 처음 받아온 위치값을 각 인스턴스에서 사용할 수 있도록 재선언
    this.xPos = info.xPos;

    // 5. 키보드 좌우키로 움직일 때 스피드 정의
    this.speed = 2;

    this.init();
}

Character.prototype = {
    constructor: Character,
    init: function () {
        const self = this;
        window.addEventListener( 'scroll', function () {
            clearTimeout( self.scrollState );

            if (!self.scrollState) {
                self.mainElem.classList.add( 'running' );
            }

            self.scrollState = setTimeout(function () {
                self.scrollState = false;
                self.mainElem.classList.remove( 'running' );
            }, 500);

            if (self.lastScrollTop > pageYOffset) {
                self.mainElem.setAttribute( 'data-direction', 'backward' );
            } else {
                self.mainElem.setAttribute( 'data-direction', 'forward' );
            }

            self.lastScrollTop = pageYOffset;
        } );

        // 1-1. 키를 눌렀을때 이벤트 등록은 keydown
        window.addEventListener('keydown', function (e) {
            // 1-2. 키코드 값 출력해 보기
            // console.log( e.keyCode );

            // 2-1. 왼쪽
            if (e.keyCode == 37) {
                self.mainElem.setAttribute( 'data-direction', 'left' );

                // 3-1. 걸어가기
                self.mainElem.classList.add( 'running' );

                // 6-2. 왼쪽이므로 빼기로
                self.xPos -= self.speed; // 애니메이션이 부드럽지 않아서 다른 방식으로 바꿔줄 필요가 있음
                // 6-3. xPos 갱신된 값으로 캐릭터 요소에 적용
                self.mainElem.style.left = self.xPos + '%';

                // 2-2. 오른쪽
            } else if (e.keyCode == 39) {
                self.mainElem.setAttribute( 'data-direction', 'right' );

                // 3-2. 걸어가기
                self.mainElem.classList.add( 'running' );
            }

        });

        // 4. 키를 뗐을때 캐릭터 멈추기
        window.addEventListener( 'keyup', function () {
            self.mainElem.classList.remove( 'running' );
        } );
    }
};


