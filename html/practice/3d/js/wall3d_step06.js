(function () {

    const stageElem = document.querySelector( '.stage' );
    const houseElem = document.querySelector( '.house' );
    const barElem = document.querySelector( '.progress-bar' );

    // 1. 캐릭터 버튼에 직접 이벤트를 등록하는 것이 아니라 부모인 컨테이너
    // select-character 로 정함
    // 컨테이너에 이벤트를 한 번만 등록해 두고 이벤트 객체의 타켓을 조사해서
    // 그 타켓이 가진 값을 이용해서 처리하는 방식(이벤트 위임)으로 한다.
    const selectCharacterElem = document.querySelector( '.select-character' );

    const mousePos = {x: 0, y: 0};
    let maxScrollValue;

    function resizeHandler() {
        maxScrollValue = document.body.offsetHeight - window.innerHeight;
    }

    window.addEventListener( 'scroll', function () {
        const scrollPer = (pageYOffset / maxScrollValue);
        const zMove = scrollPer * 980 - 490;
        houseElem.style.transform = 'translateZ('+ zMove +'vw)';
        barElem.style.width = scrollPer * 100 + '%';
    } );

    window.addEventListener( 'mousemove', function (e) {
        mousePos.x = -1 + (e.clientX / window.innerWidth) * 2;
        mousePos.y = -1 + (e.clientY / window.innerHeight) * 2;
        stageElem.style.transform = 'rotateX('+ (mousePos.y * 5) +'deg) rotateY(' + (mousePos.x * 5) + 'deg)';
    } );

    window.addEventListener( 'resize', resizeHandler );
    resizeHandler();

    // 2. 이벤트 등록(이벤트 위임방식)
    selectCharacterElem.addEventListener( 'click', function (e) {
        // 3. data 속성값을 검사
        // console.log( e.target.getAttribute('data-char') );

        // 4-1.
        let value = e.target.getAttribute('data-char');

        // 4-2.
        document.body.setAttribute( 'data-char', value );
    } );

    stageElem.addEventListener('click', function (e) {
        new Character({
            xPos : e.clientX / window.innerWidth * 100,
            speed : Math.random() * 0.5 + 0.2
        })
    })

})();

