(function () {

    const stageElem = document.querySelector( '.stage' );
    const houseElem = document.querySelector( '.house' );
    const barElem = document.querySelector( '.progress-bar' );

    const mousePos = {x: 0, y: 0};
    let maxScrollValue;

    function resizeHandler() {
        maxScrollValue = document.body.offsetHeight - window.innerHeight;
    }

    window.addEventListener( 'scroll', function () {
        const scrollPer = (pageYOffset / maxScrollValue);
        const zMove = scrollPer * 980 - 490;
        houseElem.style.transform = 'translateZ('+ zMove +'vw)';
        barElem.style.width = scrollPer * 100 + '%';
    } );

    window.addEventListener( 'mousemove', function (e) {

        mousePos.x = -1 + (e.clientX / window.innerWidth) * 2;
        mousePos.y = -1 + (e.clientY / window.innerHeight) * 2;
        stageElem.style.transform = 'rotateX('+ (mousePos.y * 5) +'deg) rotateY(' + (mousePos.x * 5) + 'deg)';
    } );

    window.addEventListener( 'resize', resizeHandler );
    resizeHandler();

    // 1-1. .stage 를 클릭할 때 마우스 위치에 따라 캐릭터 생성하기
    stageElem.addEventListener('click', function (e) {
        // 1-2. 캐릭터가 잘 생성되는지 확인
        // new Character();
        // 2. 캐릭터 left 값을 퍼센트로 설정하고 있으므로 %값으로 사용하도록 만든다.
        // console.log( e.clientX / window.innerWidth * 100 );

        // 3. 캐릭터 생성시 필요한 마우스 위치값을 생성자 함수에 전달
        new Character({
            xPos : e.clientX / window.innerWidth * 100
        })
    })

})();