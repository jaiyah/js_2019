function Character(info) {

    this.mainElem = document.createElement( 'div' );
    this.mainElem.classList.add( 'character' );

    this.mainElem.innerHTML = ''
        + '<div class="character-face-con character-head">'
            + '<div class="character-face character-head-face face-front"></div>'
            + '<div class="character-face character-head-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-torso">'
            + '<div class="character-face character-torso-face face-front"></div>'
            + '<div class="character-face character-torso-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-right">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-left">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-right">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-left">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>';

    document.querySelector( '.stage' ).appendChild(this.mainElem);

    this.mainElem.style.left = info.xPos + '%';
    this.scrollState = false;

    // 1-3. 바로 이전(마지막) 스크롤 위치
    this.lastScrollTop = 0;

    this.init();
}

Character.prototype = {
    constructor: Character,
    init: function () {
        const self = this;
        window.addEventListener( 'scroll', function () {
            clearTimeout( self.scrollState );

            if (!self.scrollState) {
                self.mainElem.classList.add( 'running' );
            }

            self.scrollState = setTimeout(function () {
                self.scrollState = false;
                self.mainElem.classList.remove( 'running' );
            }, 500);

            // 1-1. 스크롤이 내려갈때 올라갈때의 값 체크하기
            // 맨 처음에 사용자가 스크롤을 발생시키고 나서 멈추면 그때의 스크롤 위치를 기준으로 위,아래를 판단할 수 있다.
            // 즉, 스크롤 마지막 위치인 직전 값을 기준(그때의 스크롤 위치를 판단)으로
            // 스크롤중인 현재 위치(pageYOffset)보다 작으면 스크롤을 올렸다고 판단하고 더 크면 스크롤을 내렸다고 판단할 수 있다.

            // 1-3.
            console.log(' lastScrollTop : '  + self.lastScrollTop ); // 이전 값
            // 1-2. 현재 스크롤한 위치
            console.log(' pageYOffset : '  + pageYOffset ); // 최신 값

            self.lastScrollTop = pageYOffset;
        } );
    }
};
