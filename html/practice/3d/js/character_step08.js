function Character(info) {

    this.mainElem = document.createElement( 'div' );
    this.mainElem.classList.add( 'character' );

    this.mainElem.innerHTML = ''
        + '<div class="character-face-con character-head">'
            + '<div class="character-face character-head-face face-front"></div>'
            + '<div class="character-face character-head-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-torso">'
            + '<div class="character-face character-torso-face face-front"></div>'
            + '<div class="character-face character-torso-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-right">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-left">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-right">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-left">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>';

    document.querySelector( '.stage' ).appendChild(this.mainElem);

    this.mainElem.style.left = info.xPos + '%';
    this.scrollState = false;
    this.lastScrollTop = 0;
    this.xPos = info.xPos;
    this.speed = 2;

    // 3-1. 왼쪽인지 오른쪽인지 판별하기 위한 변수 선언
    this.direction;

    this.init();
}

Character.prototype = {
    constructor: Character,
    init: function () {

        const self = this;

        window.addEventListener( 'scroll', function () {
            clearTimeout( self.scrollState );

            if (!self.scrollState) {
                self.mainElem.classList.add( 'running' );
            }

            self.scrollState = setTimeout(function () {
                self.scrollState = false;
                self.mainElem.classList.remove( 'running' );
            }, 500);

            if (self.lastScrollTop > pageYOffset) {
                self.mainElem.setAttribute( 'data-direction', 'backward' );
            } else {
                self.mainElem.setAttribute( 'data-direction', 'forward' );
            }

            self.lastScrollTop = pageYOffset;
        } );

        window.addEventListener('keydown', function (e) {
            // 1. 움직임이 키다운 이벤트에 의존하고 있는데 이 이벤트가 얼마나 자주 일어나는지를 체크해 보자.
            // console.log( '키다운' );
            // 보통 애니메이션이 부드러울려면 최소 초당 24(영화),30(TV)프레임이상은 되어야 한다.
            // 키다운 이벤트는 보통 초당 10프레임정도 밖에 되지 않음

            if (e.keyCode == 37) {
                self.mainElem.setAttribute( 'data-direction', 'left' );
                self.mainElem.classList.add( 'running' );

                // 3-2. 왼쪽값 설정
                self.direction = 'left';
                self.run(self);

                // self.xPos -= self.speed;
                // self.mainElem.style.left = self.xPos + '%';

            } else if (e.keyCode == 39) {
                self.mainElem.setAttribute( 'data-direction', 'right' );
                self.mainElem.classList.add( 'running' );

                // 3-3. 오른쪽값 설정
                self.direction = 'right';
                self.run(self);
            }

        });

        window.addEventListener( 'keyup', function () {
            self.mainElem.classList.remove( 'running' );
        } );
    },
    // 2. 방향에 따른 선언과 requestAnimationFrame 선언
    run : function (self) {

        // 4-1. 방향에 따른 스피드,위치
        if (self.direction == 'left') {
            self.xPos -= self.speed;
        } else if (self.direction == 'right') {
            self.xPos += self.speed;
        }

        // 4-2.
        self.mainElem.style.left = self.xPos + '%';

        // 5. this 값 에러
        // requestAnimationFrame(self.run)

        // 6. this 값 해결
        requestAnimationFrame(function () {
            self.run( self );
        })
        // 7. 다음 챕터에서 애니메이션 속도 문제를 해결해 보자.
    }
};
