function Character(info) {

    this.mainElem = document.createElement( 'div' );
    this.mainElem.classList.add( 'character' );

    this.mainElem.innerHTML = ''
        + '<div class="character-face-con character-head">'
            + '<div class="character-face character-head-face face-front"></div>'
            + '<div class="character-face character-head-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-torso">'
            + '<div class="character-face character-torso-face face-front"></div>'
            + '<div class="character-face character-torso-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-right">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
            + '<div class="character-face-con character-arm character-arm-left">'
            + '<div class="character-face character-arm-face face-front"></div>'
        + '<div class="character-face character-arm-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-right">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>'
        + '<div class="character-face-con character-leg character-leg-left">'
            + '<div class="character-face character-leg-face face-front"></div>'
            + '<div class="character-face character-leg-face face-back"></div>'
        + '</div>';

    document.querySelector( '.stage' ).appendChild(this.mainElem);

    this.mainElem.style.left = info.xPos + '%';
    this.scrollState = false;
    this.lastScrollTop = 0;
    this.xPos = info.xPos;

    // 3-1. 스피드를 앞전까지 고정으로 해놨기 때문에 이를 랜덤하게 설정
    // this.speed = 0.3;
    this.speed = info.speed;

    this.direction;
    this.runningState = false;
    this.rafId;

    this.init();
}

Character.prototype = {
    constructor: Character,
    init: function () {

        const self = this;

        window.addEventListener( 'scroll', function () {
            clearTimeout( self.scrollState );

            if (!self.scrollState) {
                self.mainElem.classList.add( 'running' );
            }

            self.scrollState = setTimeout(function () {
                self.scrollState = false;
                self.mainElem.classList.remove( 'running' );
            }, 500);

            if (self.lastScrollTop > pageYOffset) {
                self.mainElem.setAttribute( 'data-direction', 'backward' );
            } else {
                self.mainElem.setAttribute( 'data-direction', 'forward' );
            }

            self.lastScrollTop = pageYOffset;
        } );

        window.addEventListener('keydown', function (e) {

            if (self.runningState) return;

            if (e.keyCode == 37) {
                self.mainElem.setAttribute( 'data-direction', 'left' );
                self.mainElem.classList.add( 'running' );
                self.direction = 'left';
                self.run(self);
                self.runningState = true;

            } else if (e.keyCode == 39) {
                self.mainElem.setAttribute( 'data-direction', 'right' );
                self.mainElem.classList.add( 'running' );
                self.direction = 'right';
                self.run(self);
                self.runningState = true;
            }

        });

        window.addEventListener( 'keyup', function () {
            self.mainElem.classList.remove( 'running' );
            cancelAnimationFrame( self.rafId );
            self.runningState = false;
        } );
    },
    run : function (self) {


        if (self.direction === 'left') {
            self.xPos -= self.speed;
        } else if (self.direction === 'right') {
            self.xPos += self.speed;
        }

        // 1. 좌우 이동시 양벽을 뚫고 나가기 때문에 이를 해결
        // 여기서 설정한 xPos 값으로 제어해보도록 하자.
        // console.log( self.xPos );

        // 2-1. 좌우 끝까지 갔을 때의 적절한 값을 확인한 후 조건문
        // xPos 값이 위에서 계산된 후에 조건문을 걸어주는 것이 적절함
        if (self.xPos < 2) {
            self.xPos = 2;
        }
        if (self.xPos > 88) {
            self.xPos = 88;
        }

        // 2-2. 양쪽벽에 부딪힌 후 캐릭터(인스턴스객체)들이 몰려다닌다.
        // 이 캐릭터들의 속도를 랜덤하게 설정해 보자.

        self.mainElem.style.left = self.xPos + '%';

        self.rafId = requestAnimationFrame(function () {
            self.run( self );
        })
    }
};


