(function () {

    // 4-2. 시점에 따른 회전시킬 요소
    const stageElem = document.querySelector( '.stage' );

    const houseElem = document.querySelector( '.house' );
    const barElem = document.querySelector( '.progress-bar' );

    // 3-2. 마우스 위치값을 제어할 객체를 생성
    const mousePos = {x: 0, y: 0};
    let maxScrollValue;

    function resizeHandler() {
        maxScrollValue = document.body.offsetHeight - window.innerHeight;
    }

    window.addEventListener( 'scroll', function () {
        const scrollPer = (pageYOffset / maxScrollValue);
        const zMove = scrollPer * 980 - 490;
        houseElem.style.transform = 'translateZ('+ zMove +'vw)';

        // progress bar
        barElem.style.width = scrollPer * 100 + '%';
    } );

    // 마우스 움직에 따라 3d 벽면 움직이기
    // 1. 마우스 이벤트 등록
    window.addEventListener( 'mousemove', function (e) {
        // 2. 마우스의 위치 알아내기
        // console.log( e.clientX, e.clientY ); // x, y 좌표 (값은 px로 얻어옴)

        // 3-1. 가운데있을 경우에는 0도로 회전, 좌,우시점에 따라 회전
        // 가운데를 0으로 기준을 잡고 상하좌우일때 +1, -1 로 부호만 바뀔 때 회전을 시켜주도록 한다.

        // 3-3. clientX, clientY 개발하기 편하도록 가공하기
        // e.clientX / window.innerWidth => 0과 0.999사이의 값
        // console.log( e.clientX / window.innerWidth );
        // 가운데가 0.5 이므로 *2를 해줘서 오른쪽일경우 1, 왼족일경우 0의 값
        // console.log( e.clientX / window.innerWidth * 2 );
        // 가운데가 곱하기 2를 하여 1이므로 마이너스 1을 더해주면 그 값은 양수를 유지하고
        // 왼쪽인 경우에는 음수값이 되므로 다음과 같은 식을 사용한다.
        // console.log(-1 + e.clientX / window.innerWidth * 2 );

        mousePos.x = -1 + (e.clientX / window.innerWidth) * 2;
        mousePos.y = -1 + (e.clientY / window.innerHeight) * 2;
        // console.log( mousePos );

        // 4-1. 시점 변경에 따라서 캐릭터들도 변경되야 함으로 stage 에 mousePos 값을 적용한다.
        // stageElem.style.transform = 'rotateX(0deg) rotateY(0deg)';
        // stageElem.style.transform = 'rotateX('+ 0 +'deg) rotateY(' + 0 + 'deg)';
        // 회전은 축을 기준으로 회전하기 때문에 마우스 x,y 와는 반대로 설정
        // stageElem.style.transform = 'rotateX('+ mousePos.y +'deg) rotateY(' + mousePos.x + 'deg)';

        // -1 ~ 1 의 값으로 각도가 작으므로 좀더 효과를 주기위해 각도를 증가시켜준다.
        stageElem.style.transform = 'rotateX('+ (mousePos.y * 5) +'deg) rotateY(' + (mousePos.x * 5) + 'deg)';
    } );


    window.addEventListener( 'resize', resizeHandler );
    resizeHandler();

})();