// 전역변수 공간을 사용하지 않기 위해 즉시실행 함수 선언
(function () {

    // 1. 스크롤시 벽들을 이동시킨다.
    // 벽을 이동시키려면 부모인 house 를 이동시켜야 한다.
    const houseElem = document.querySelector( '.house' );

    // 스크롤 이벤트 핸들러 등록
    window.addEventListener( 'scroll', function () {
        // 스크롤된 양에 따라 움직여야 하므로 얼마만큼 스크롤이 된지를 체크하기 위해
        // 스크롤을 수치화시켜야 한다.
        console.log( window.pageYOffset );

        // 스크롤 수치는 브라우저 창크기에 마지막 스크롤 값이 달라지기 때문에
        // 스크롤이 많이 되든 적게 되든 그에 대한 스크롤의 범위를 알 필요가 있다.
        // 즉, 실제 스크롤되는 범위를 구하고 거기서 얼마나 스크롤되는지를 비율로 구해서 기능을 처리하려고 한다.

        // 문서의 높이에서 윈도우의 높이(현재 뷰포트 높이)를 빼면 그것이 전체 스크롤할 수 있는 범위가 된다.
        // window.innerHeight : 현재 창 사이즈
        // 전체 스크롤할 수 있는 범위의 값은 아래와 같다.
        console.log( document.body.offsetHeight - window.innerHeight );

    } );

})();


