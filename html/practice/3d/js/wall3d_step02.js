(function () {

    const houseElem = document.querySelector( '.house' );

    let maxScrollValue = document.body.offsetHeight - window.innerHeight;

    // 스크롤 범위 갱신되는 경우
    function resizeHandler() {
        maxScrollValue = document.body.offsetHeight - window.innerHeight;
    }

    window.addEventListener( 'scroll', function () {
        const zMove = (window.pageYOffset / maxScrollValue) * 980 - 490;
        houseElem.style.transform = 'translateZ('+ zMove +'vw)'
    } );

    // 1. 창 사이즈가 변경되었을 경우 스크롤 범위를 다시 갱신하기
    window.addEventListener( 'resize', resizeHandler );

})();


